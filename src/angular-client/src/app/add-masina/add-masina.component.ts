import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CarServiceService } from '../car-service.service';

@Component({
  selector: 'app-add-masina',
  templateUrl: './add-masina.component.html',
  styleUrls: ['./add-masina.component.css']
})
export class AddMasinaComponent implements OnInit, OnDestroy{

  public carFrom!: FormGroup;

  constructor(private carservice:CarServiceService){}


  private createForm(){

    this.carFrom = new FormGroup({

      brand: new FormControl(null,[
        Validators.min(1),
        Validators.required,
      ]),

      year: new FormControl(null,[
        Validators.min(1),
        Validators.required,
      ]),

      price: new FormControl(null,[
        Validators.min(1),
        Validators.required,
      ])
    },
    {
      updateOn:'change'
    })


  }

  public addCar(){

    if(this.carFrom.valid){

      this.carservice.addCar(this.carFrom.value).subscribe(data=>{
        console.log(data);
      });
    }

    console.log(this.carFrom.value);


  }




  ngOnDestroy(): void {
    
  }
  ngOnInit(): void {

    this.createForm();
    
  }

}
