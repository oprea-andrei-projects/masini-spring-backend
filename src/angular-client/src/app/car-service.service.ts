import { Injectable } from '@angular/core';
import { environment } from './environments/environment';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { BehaviorSubject, catchError, Observable, of, tap } from 'rxjs';
import { Masina } from './models/masina';

@Injectable({
  providedIn: 'root'
})
export class CarServiceService {

 

  private server = environment.apiUrl;

  constructor(private http: HttpClient) {
   
  }


  getCars(): Observable<Array<Masina>> {
    return this.http.get<Array<Masina>>(this.server+"/allMasini").pipe(
      catchError(this.handleError)
    );
  }

  addCar(car:Masina): Observable<Masina>{
    return this.http.post<Masina>(this.server+"/addMasina",car);
  }


  private handleError(error: HttpErrorResponse): Observable<any> {

    console.log(error);

    let errorMessage: string;
    if (error.error instanceof ErrorEvent) {
      errorMessage = `Client Error - ${error.error.message}`;

    } else {

      if (error.error.reason) {
        errorMessage = `${error.error.reason} - Error code ${error.status}`;
      } else {
        errorMessage = `An error occurred - Error code ${error.status}`;
      }
    }
    return of(errorMessage);

  }


}
