import { Component, OnDestroy, OnInit } from '@angular/core';
import { Masina } from '../models/masina';
import { CarServiceService } from '../car-service.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy{

  public cars:Array<Masina> = [];
  constructor(private carService: CarServiceService){

  }


  ngOnDestroy(): void {
    throw new Error('Method not implemented.');
  }

  
  ngOnInit(): void {
    this.carService.getCars().subscribe(data=>{
      this.cars = data;
      console.log(data);
    })
  }

}
